package people.luozi.com.androidpeople;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.AdapterView;
import android.widget.GridView;


public class MainActivity extends Activity {


    private Activity mActivity;
    private CallHtmlHelper mCallHtmlHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mActivity = this;

        GridView gridView = (GridView) findViewById(R.id.content_list);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                Intent intent = new Intent(mActivity, DetailedActivity.class);
                intent.putExtra(Utils.KEY_URL, mCallHtmlHelper.getUrl(position));
                startActivity(intent);
            }
        });

        mCallHtmlHelper = new CallHtmlHelper(this, gridView);

    }

}
