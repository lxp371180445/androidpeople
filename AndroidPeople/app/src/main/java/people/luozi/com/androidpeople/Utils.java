package people.luozi.com.androidpeople;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.DisplayMetrics;
import android.widget.Toast;

public class Utils {

    public static final String KEY_URL = "KEY_URL";

    /**
     * 屏幕宽度,单位像素(px).
     */
    public static int getWidth(Context context) {
        DisplayMetrics displayMetrics = getDisplayMetrics(context);
        return displayMetrics.widthPixels;
    }

    /**
     * 屏幕高度,单位像素(px).
     */
    public static int getHeight(Context context) {
        DisplayMetrics displayMetrics = getDisplayMetrics(context);
        return displayMetrics.heightPixels;
    }

    // 屏幕像素对象
    private static DisplayMetrics getDisplayMetrics(Context context) {
        return context.getResources().getDisplayMetrics();
    }

    /**
     * 显示用户交互等待提示框,没有标题.
     */
    public static ProgressDialog show(Context context, CharSequence message) {
        return show(context, null, message);
    }

    /**
     * 显示用户交互等待提示框.
     */
    public static ProgressDialog show(Context context, CharSequence title, CharSequence message) {
        ProgressDialog mPd = ProgressDialog.show(context, title, message);

        mPd.setCancelable(true);
        mPd.setIndeterminate(false);
        mPd.setCanceledOnTouchOutside(false);

        return mPd;
    }

    /**
     * 弹出长时间的Toast提示框.
     */
    public static void showLongToast(Context c, String text) {
        Toast.makeText(c, text, Toast.LENGTH_LONG).show();
    }

    /**
     * 弹出长时间的Toast提示框.
     */
    public static void showLongToast(Context c, int resId) {
        Toast.makeText(c, resId, Toast.LENGTH_LONG).show();
    }

    /**
     * 弹出长时间的Toast提示框.
     */
    public static void showShortToast(Context c, String text) {
        Toast.makeText(c, text, Toast.LENGTH_SHORT).show();
    }

    /**
     * 弹出长时间的Toast提示框.
     */
    public static void showShortToast(Context c, int resId) {
        Toast.makeText(c, resId, Toast.LENGTH_SHORT).show();
    }

}
