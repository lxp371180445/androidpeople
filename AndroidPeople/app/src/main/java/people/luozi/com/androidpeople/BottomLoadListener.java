package people.luozi.com.androidpeople;

import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;

public class BottomLoadListener implements OnScrollListener {

	public interface BottomLoadCallBack {
		void execute();
	}

	private int getLastVisiblePosition = 0, lastVisiblePositionY = 0;
	private BottomLoadCallBack mCallback;

	public BottomLoadListener(BottomLoadCallBack callback) {
		this.mCallback = callback;
	}

	public void onScrollStateChanged(AbsListView view, int scrollState) {

		if (scrollState == OnScrollListener.SCROLL_STATE_IDLE) {
			if (view.getLastVisiblePosition() == (view.getCount() - 1)) {
				View childView = (View) view.getChildAt(view.getChildCount() - 1);
				int[] location = new int[2];
				childView.getLocationOnScreen(location);
				int y = location[1];

				if (view.getLastVisiblePosition() != getLastVisiblePosition
						&& lastVisiblePositionY != y) {
					Utils.showShortToast(view.getContext(), "已到底部，再次上拉即可加载更多");
					getLastVisiblePosition = view.getLastVisiblePosition();
					lastVisiblePositionY = y;
					return;
				} else if (view.getLastVisiblePosition() == getLastVisiblePosition
						&& lastVisiblePositionY == y) {
					mCallback.execute();
				}
			}

			getLastVisiblePosition = 0;
			lastVisiblePositionY = 0;
		}
	}

	public void onScroll(AbsListView arg0, int arg1, int arg2, int arg3) {

	}
}