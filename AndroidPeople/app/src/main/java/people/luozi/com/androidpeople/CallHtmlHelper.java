package people.luozi.com.androidpeople;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

public class CallHtmlHelper {

    Activity mActivity;
    GridView mGridView;
    CallService.PeopleAdapter mPeopleAdapter;
    ArrayList<String> mImgList;
    ArrayList<String> mUrlList;

    int mPage = 1;

    public CallHtmlHelper(Activity activity, GridView gridView) {

        this.mActivity = activity;
        this.mGridView = gridView;
        mImgList = new ArrayList<String>();
        mUrlList = new ArrayList<String>();

        startService();

        BottomLoadListener.BottomLoadCallBack callBack = new BottomLoadListener.BottomLoadCallBack() {

            public void execute() {
                mPage++;
                startService();
            }
        };
        BottomLoadListener bottomLoadListener = new BottomLoadListener(callBack);
        mGridView.setOnScrollListener(bottomLoadListener);
    }

    public void startService() {
        new CallService().execute();
    }

    public String getUrl(int index) {
        return mPeopleAdapter.getUrl(index);
    }

    class CallService extends AsyncTask<Integer, String, String> {

        @Override
        protected String doInBackground(Integer... params) {
            try {
                Document doc = Jsoup.connect("http://www.avsow.com/cn/popular/currentPage/" + mPage).timeout(5000).get();
                Elements newsHeadlines = doc.select("#waterfall");
                Document divcontions = Jsoup.parse(newsHeadlines.toString());
                Elements element = divcontions.getElementsByTag("div");
                Elements as = element.select("a");
                Elements imgs = element.select("img");

                int size = imgs.size();
                for (int index = 0; index < size; index++) {
                    String img = imgs.get(index).getElementsByTag("img").attr("src").trim();
                    String url = as.get(index * 2).getElementsByTag("a").attr("href").trim();

                    mImgList.add(img);
                    mUrlList.add(url);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (mPage == 1) {
                mGridView.setAdapter(mPeopleAdapter = new PeopleAdapter(mImgList, mUrlList));
            } else {
                mPeopleAdapter.notifyDataSetChanged();
            }
        }


        class PeopleAdapter extends BaseAdapter {

            public ArrayList<String> mImgList;
            public ArrayList<String> mUrlList;

            LayoutInflater mInflater;
            int mViewWidth;
            int mViewHeight;

            public PeopleAdapter(ArrayList<String> imgList, ArrayList<String> urlList) {
                this.mImgList = imgList;
                this.mUrlList = urlList;

                mViewWidth = Utils.getWidth(mActivity) / 3;
                mViewHeight = mViewWidth * 4 / 3;
                mInflater = LayoutInflater.from(mActivity);
            }

            String getUrl(int index) {
                return this.mUrlList.get(index);
            }

            public void setModel(ArrayList<String> imgList, ArrayList<String> urlList) {
                this.mImgList.addAll(imgList);
                this.mUrlList.addAll(urlList);
                notifyDataSetChanged();
            }

            @Override
            public int getCount() {
                return mImgList.size();
            }

            @Override
            public Object getItem(int position) {
                return null;
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public View getView(int position, View v, ViewGroup parent) {
                ViewHolder holder;
                if (v == null) {
                    v = mInflater.inflate(R.layout.item_people, null);
                    holder = new ViewHolder();
                    holder.img = (ImageView) v.findViewById(R.id.img);
                    v.setTag(holder);
                } else {
                    holder = (ViewHolder) v.getTag();
                }
                LinearLayout.LayoutParams l = new LinearLayout.LayoutParams(mViewWidth, mViewHeight);
                l.leftMargin = 20;
                l.rightMargin = 20;
                l.topMargin = 20;
                l.bottomMargin = 20;
                holder.img.setLayoutParams(l);


                String img = mImgList.get(position);
                Picasso.with(mActivity).load(img).into(holder.img);
                return v;
            }

            class ViewHolder {
                ImageView img;
            }

        }
    }

}
