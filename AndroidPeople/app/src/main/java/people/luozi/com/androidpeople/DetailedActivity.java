package people.luozi.com.androidpeople;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DetailedActivity extends Activity {

    private Activity mActivity;

    private ImageView mIconImageView;
    private TextView mTitleTextView;

    private List<String[]> mContentList;
    private ListView mContentListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_detailed);

        mActivity = this;
        mIconImageView = (ImageView) findViewById(R.id.icon);
        mTitleTextView = (TextView) findViewById(R.id.title);
        mContentListView = (ListView) findViewById(R.id.content_list);

        mContentList = new ArrayList<>();

        mIconImageView.setLayoutParams(new LinearLayout.LayoutParams(Utils.getWidth(this) - 10,
                (Utils.getWidth(this) - 10) * 2 / 3));


        String url = getIntent().getStringExtra(Utils.KEY_URL);
        new CallService().execute(url);

    }

    class CallService extends AsyncTask<String, String, String[]> {

        @Override
        protected String[] doInBackground(String... params) {
            String[] strings = new String[4];
            try {
                Document doc = Jsoup.connect(params[0]).timeout(5000).get();
                Elements download = doc.select("#download");
                Document divcontions = Jsoup.parse(download.toString());

                Elements element = divcontions.getElementsByTag("a");
                Elements imgElement = doc.getElementsByTag("img");
                Elements spanElement = doc.getElementsByTag("span");


                strings[0] = imgElement.get(0).attr("src").trim();
                strings[1] = imgElement.get(0).attr("title").trim();
                strings[2] = spanElement.get(2).text();
                strings[3] = element.get(0).attr("href").trim();

            } catch (IOException e) {
                e.printStackTrace();
            }

            return strings;
        }

        @Override
        protected void onPostExecute(String[] strings) {
            super.onPostExecute(strings);
            Picasso.with(mActivity).load(strings[0]).into(mIconImageView);
            mTitleTextView.setText(strings[1]);
            setTitle(strings[2]);


            new ListCallService().execute(strings[3]);
        }
    }


    class ListCallService extends AsyncTask<String, String, String[]> {

        @Override
        protected String[] doInBackground(String... params) {
            try {
                Document doc = Jsoup.connect(params[0]).timeout(5000).get();
                Elements element = doc.getElementsByTag("tbody");

                Elements trElements = element.get(0).select("tr");
                int size = trElements.size();
                for (int index = 0; index < size; index++) {
                    Elements tdElements = trElements.get(index).getElementsByTag("td");

                    Elements info = tdElements.get(0).select("a");
                    String[] values = new String[4];
                    values[0] = info.get(0).attr("href").trim();
                    values[1] = info.get(0).attr("title").trim();
                    values[2] = tdElements.get(1).text();
                    values[3] = tdElements.get(2).text();
                    mContentList.add(values);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String[] strings) {
            super.onPostExecute(strings);

            ContentAdapter adapter = new ContentAdapter(mActivity);
            adapter.addInfoList(mContentList);
            mContentListView.setAdapter(adapter);
        }
    }

    class ContentAdapter extends BaseListAdapter<String[]> {

        public ContentAdapter(Context context) {
            super(context, R.layout.item_content);
        }

        @Override
        protected void setModel(int position, View[] views) {
            String[] values = getItem(position);
            ((TextView)views[0]).setText(values[1]);
            ((TextView)views[1]).setText(values[2]);
            ((TextView)views[2]).setText(values[3]);
        }

        @Override
        protected ViewHolder getViewHolder(View convertView) {
            return new ViewHolder(convertView, new int[]{R.id.title, R.id.size, R.id.date});
        }
    }
}
